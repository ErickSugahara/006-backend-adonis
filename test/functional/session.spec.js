const { test, trait } = use('Test/Suite')('Session');

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');

trait('Test/ApiClient');

test('if should return JWT token when session created', async ({
  assert,
  client,
}) => {
  const sessionPayload = {
    email: 'erick.sugahara@gmail.com',
    password: '123456',
  };

  await Factory.model('App/Models/User').create(sessionPayload);

  const response = await client
    .post('/sessions')
    .send({
      email: 'erick.sugahara@gmail.com',
      password: '123456',
    })
    .end();

  response.assertStatus(200);
  assert.exists(response.body.token);
});
